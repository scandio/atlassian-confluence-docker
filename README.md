# Docker for Atlassian Confluence

[![](https://badge.imagelayers.io/scandio/atlassian-confluence:latest.svg)](https://imagelayers.io/?images=scandio/atlassian-confluence:latest 'Get your own badge on imagelayers.io')

Quick Run with:
    
	docker run -d -p 8090:8090 scandio/atlassian-confluence
    
If you're using OSX you first need to find out your boot2docker IP before you can access Confluence:

	boot2docker ip
    
Now open http://[Boot2docker IP]:8090 in your browser.

On a live environment we are using nginx from jwilder 

https://registry.hub.docker.com/u/jwilder/nginx-proxy/

So to run an conflunece at your own url just run th jwilder/nginx-proxy and use the following command:

	docker run -d -VIRTUAL_HOST=subdomain.domain.tld -VIRTUAL_PORT=8090 scandio/atlassian-confluence

If you need a persistant file system and mysql database for your confluence use:

	#mysql server
	docker run --name yourmysql -v /data/mysql/conf:/etc/mysql/conf.d -v /data/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=[yourpasswordhere] -d mysql:latest

	#data container
	docker run -u root --name data-confluence -v /dockerData/confluenceHome:/var/local/atlassian/confluence scandio/confluence-mysql chown -R daemon /var/local/atlassian/confluence

	#confluence with persistant storage and connected database
	docker run -v /data/confluence:/var/local/atlassian/confluence --link yourmysql:db --d -e RAM_MIN=2048 -e RAM_MAX=4096 -e VIRTUAL_HOST=subdomain.yourdomain.tld scandio/atlassian-confluence
	
If you want to connect to the server using remote debugging in your environ

	docker run -p 8090:8090 -p 5005:5005 -e CONF_REMOTE_DEBUG=true -d -e RAM_MIN=2048 -e RAM_MAX=4096 scandio/atlassian-confluence